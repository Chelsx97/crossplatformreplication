#include "Rock.h"
#include "Maths.h"
#include "InputState.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;

Rock::Rock() :
	GameObject(),
	mVelocity(Vector3::Zero),
	mMaxLinearSpeed(50.f),
	mMaxRotationSpeed(5.f),
	mWallRestitution(0.1f),
	mNPCRestitution(0.1f),
	mRockId(0)


{
	SetCollisionRadius(0.5f);
}

void Rock::Update()
{

}

uint32_t Rock::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & ECRS_RockId)
	{
		inOutputStream.Write((bool)true);
		inOutputStream.Write(GetRockId());

		writtenState |= ECRS_RockId;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}


	if (inDirtyState & ECRS_Pose)
	{
		inOutputStream.Write((bool)true);

		Vector3 velocity = mVelocity;
		inOutputStream.Write(velocity.mX);
		inOutputStream.Write(velocity.mY);

		Vector3 location = GetLocation();
		inOutputStream.Write(location.mX);
		inOutputStream.Write(location.mY);

		inOutputStream.Write(GetRotation());

		writtenState |= ECRS_Pose;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	
	if (inDirtyState & ECRS_Color)
	{
		inOutputStream.Write((bool)true);
		inOutputStream.Write(GetColor());

		writtenState |= ECRS_Color;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	

	return writtenState;
}

//void Rock::ProcessCollisions()
//{
//	//right now just bounce off the sides..
//	ProcessCollisionsWithScreenWalls();
//
//	float sourceRadius = GetCollisionRadius();
//	Vector3 sourceLocation = GetLocation();
//
//	//now let's iterate through the world and see what we hit...
//	//note: since there's a small number of objects in our game, this is fine.
//	//but in a real game, brute-force checking collisions against every other object is not efficient.
//	//it would be preferable to use a quad tree or some other structure to minimize the
//	//number of collisions that need to be tested.
//	for (auto goIt = World::sInstance->GetGameObjects().begin(), end = World::sInstance->GetGameObjects().end(); goIt != end; ++goIt)
//	{
//		GameObject* target = goIt->get();
//		if (target != this && !target->DoesWantToDie())
//		{
//			//simple collision test for spheres- are the radii summed less than the distance?
//			Vector3 targetLocation = target->GetLocation();
//			float targetRadius = target->GetCollisionRadius();
//
//			Vector3 delta = targetLocation - sourceLocation;
//			float distSq = delta.LengthSq2D();
//			float collisionDist = (sourceRadius + targetRadius);
//			if (distSq < (collisionDist * collisionDist))
//			{
//				//first, tell the other guy there was a collision with a player, so it can do something...
//
//				if (target->HandleCollisionWith(this))
//				{
//					//okay, you hit something!
//					//so, project your location far enough that you're not colliding
//					Vector3 dirToTarget = delta;
//					dirToTarget.Normalize2D();
//					Vector3 acceptableDeltaFromSourceToTarget = dirToTarget * collisionDist;
//					//important note- we only move this cat. the other cat can take care of moving itself
//					SetLocation(targetLocation - acceptableDeltaFromSourceToTarget);
//
//
//					Vector3 relVel = mVelocity;
//
//					//if other object is another player, it might have velocity, so there might be relative velocity...
//					Rock* otherPlayer = dynamic_cast<Rock*>(target);
//					if (otherPlayer)
//					{
//						relVel -= otherPlayer->mVelocity;
//					}
//
//					//got vel with dir between objects to figure out if they're moving towards each other
//					//and if so, the magnitude of the impulse ( since they're both just balls )
//					float relVelDotDir = Dot2D(relVel, dirToTarget);
//
//					if (relVelDotDir > 0.f)
//					{
//						Vector3 impulse = relVelDotDir * dirToTarget;
//
//						if (otherPlayer)
//						{
//							mVelocity -= impulse;
//							mVelocity *= mNPCRestitution;
//						}
//						else
//						{
//							mVelocity -= impulse * 2.f;
//							mVelocity *= mWallRestitution;
//						}
//
//					}
//				}
//			}
//		}
//	}
//}

bool Rock::operator==(Rock& other)
{
	// Game Object Part.
	//Call the == of the base, Player reference is
	//downcast explicitly.
	if (!GameObject::operator==(other)) return false;

	if (this->ECRS_AllState != other.ECRS_AllState) return false;


	if (this->mRockId != other.mRockId) return false;


	return true;
}