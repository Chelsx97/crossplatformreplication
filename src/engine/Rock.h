#ifndef Rock_H_
#define Rock_H_

#include "GameObject.h"
#include "World.h"

class InputState;

/* We'll later create client and server versions of this class */

class Rock : public GameObject
{
public:
	CLASS_IDENTIFICATION('ROCK', GameObject)

		enum ERockReplicationState
	{
		ECRS_Pose = 1 << 0,
		ECRS_Color = 1 << 1,
		ECRS_RockId = 1 << 2,
		ECRS_Health = 1 << 3,

		ECRS_AllState = ECRS_Pose | ECRS_Color | ECRS_RockId | ECRS_Health
	};

	static	GameObject* StaticCreate() { return new Rock(); }

	static	GameObjectPtr	StaticCreatePtr() { return GameObjectPtr(new Rock()); }

	virtual uint32_t GetAllStateMask()	const override { return ECRS_AllState; }

	virtual void Update() override;

	void		SetRockId(uint32_t inRockId) { mRockId = inRockId; }
	uint32_t	GetRockId()						const { return mRockId; }


	uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;

	bool operator==(Rock& other);

protected:

	Rock();


private:

	void	AdjustVelocityByThrust(float inDeltaTime);

	Vector3				mVelocity;


	float				mMaxLinearSpeed;
	float				mMaxRotationSpeed;

	uint32_t			mRockId;

	float				mWallRestitution;
	float				mNPCRestitution;

};

typedef shared_ptr< Rock >	RockPtr;

#endif // ROCK_H_